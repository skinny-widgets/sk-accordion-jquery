# Skinny Widgets Accordion for Jquery Theme

accordion element

```
npm i sk-accordion sk-accordion-jquery --save
```

then add the following to your html

```html
<sk-config
    theme="jquery"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-jquery"
></sk-config>
<sk-accordion id="accordion">
    <sk-tab title="foo">
        some foo tab contents
    </sk-tab>
    <sk-tab title="bar">
        some bar tab contents
    </sk-tab>
    <sk-tab title="baz">
        some baz tab contents
    </sk-tab>
</sk-accordion>
<script type="module">
    import { SkAccordion } from './node_modules/sk-accorion/index.js';

    customElements.define('sk-accordion', SkAccordion);
</script>
```
#### attributes

**disabled** - all tabs disabled

**mono** - open only one tab at once

#### template

id: SkAccordionTpl

