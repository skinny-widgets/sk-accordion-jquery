
import { SkAccordionImpl }  from '../../sk-accordion/src/impl/sk-accordion-impl.js';

export class JquerySkAccordion extends SkAccordionImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'accordion';
    }

    get contentEl() {
        if (! this._contentEl) {
            this._contentEl = this.comp.el.querySelector('[role=tablist]');
        }
        return this._contentEl;
    }

    set contentEl(el) {
        this._contentEl = el;
    }

    get subEls() {
        return [ 'contentEl' ];
    }

    bindTabSwitch() {
        this.contentEl.querySelectorAll('.ui-accordion-header').forEach(function(link) {
            link.onclick = function(event) {
                if (this.comp.getAttribute('disabled')) {
                    return false;
                }
                let tabId = event.target.getAttribute('data-tab');
                this.toggleTab(tabId);
            }.bind(this);
        }.bind(this));
    }

    closeAllTabs() {
        let tabs = this.contentEl.querySelectorAll('.ui-accordion-header');
        for (let tab of tabs) {
            let tabNum = tab.dataset.tab;
            this.closeTab(tabNum);
        }
    }

    closeTab(tabNum) {
        let tab = this.tabs['tabs-' + tabNum];
        if (tab.hasAttribute('open')) {
            tab.removeAttribute('open');
            let activeTab = this.contentEl.querySelector(`#header-${tabNum}`);
            activeTab.classList.remove('accordion-header-active', 'ui-state-active');
            let triIcon = activeTab.querySelector('.ui-accordion-header-icon');
            triIcon.classList.add('ui-icon-triangle-1-e');
            triIcon.classList.remove('ui-icon-triangle-1-s');
            let activeTabPane = this.contentEl.querySelector(`#panel-${tabNum}`);
            activeTabPane.style.display = 'none';
            this.selectedTab = null;
        }
    }

    toggleTab(tabNum) {
        let tab = this.tabs['tabs-' + tabNum];
        if (tab.hasAttribute('open')) {
            this.closeTab(tabNum);
        } else {
            if (this.comp.hasAttribute('mono')) {
                if (this.selectedTab) {
                    this.closeTab(this.selectedTab);
                } else {
                    this.closeAllTabs();
                }
            }
            let activeTab = this.contentEl.querySelector(`#header-${tabNum}`);
            activeTab.classList.add('accordion-header-active', 'ui-state-active');
            let triIcon = activeTab.querySelector('.ui-accordion-header-icon');
            triIcon.classList.remove('ui-icon-triangle-1-e');
            triIcon.classList.add('ui-icon-triangle-1-s');
            let activeTabPane = this.contentEl.querySelector(`#panel-${tabNum}`);
            activeTabPane.style.display = 'block';
            this.selectedTab = tabNum;
            tab.setAttribute('open', '');
        }
    }

    renderTabs() {
        let tabs = this.comp.el.querySelectorAll('sk-tab');
        let num = 1;
        this.tabs = {};
        for (let tab of tabs) {
            let isOpen = tab.hasAttribute('open');
            let title = tab.getAttribute('title') ? tab.getAttribute('title') : '';
            this.contentEl.insertAdjacentHTML('beforeend', `
                <h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons${isOpen ? ' accordion-header-active state-active' : ''}" 
                    role="tab" id="header-${num}" aria-controls="ui-accordion-mainMenu-panel-0" 
                    aria-selected="${! isOpen ? "true" : "false"}" aria-expanded="${! isOpen ? "true" : "false"}" tabindex="${num - 1}" data-tab="${num}" ${isOpen ? 'open' : ''}>
                    <span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>${title}</h3>
                <div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" ${! isOpen ? 'style="display: none;"' : ''}  
                    id="panel-${num}" aria-labelledby="header-${num}" role="tabpanel" aria-hidden="${! isOpen ? "true" : "false"}">
                    ${tab.outerHTML}
                </div>        
            `);

            this.removeEl(tab);
            this.tabs['tabs-' + num] = this.contentEl.querySelector('#header-' + num);
            num++;
        }
    }

    disable() {
        super.enable();
        this.contentEl.querySelectorAll('.ui-accordion-header').forEach((tab) => {
            tab.classList.add('ui-state-disabled');
        });
    }

    enable() {
        super.disable();
        this.contentEl.querySelectorAll('.ui-accordion-header').forEach((tab) => {
            tab.classList.add('ui-state-disabled');
        });
    }
}
